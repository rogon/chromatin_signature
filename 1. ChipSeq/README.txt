## DiffBind_DESeq2_All_binding_sites_results.txt : DEseq2 diff. binding analysis complete results
 (all peaks ie no selection). Interestin columns for you should be the chr, start, end, Fold, p.value and FDR

## gencode.vM10.TSS.1Kb.bed  : list of TSS augmented to 1Kb on each side


## deseq2_diffbound_negFC.bed and deseq2_diffbound_posFC.bed
regions with FDR < 0.05 splitted in the one with Fold > 0 and Fold < 0 (ie posFC and negFC respectively)
the format is encode broadpeak ie https://genome.ucsc.edu/FAQ/FAQformat.html#format13
ie the Fold is in col 7, the -log10(p) and -log10(fdr) in col 8 and 9 

## deseq2_diffbound_negFC_at_TSS_1Kb.bed and deseq2_diffbound_posFC_at_TSS_1Kb.bed
same as above but only peaks found at TSS i.e. overlapping the gencode.vM10.TSS.1Kb.bed 

##  deseq2_diffbound_negFC_at_TSS_1Kb.fasta and deseq2_diffbound_posFC_at_TSS_1Kb.fasta
sequence in fasta for deseq2_diffbound_negFC_at_TSS_1Kb.bed and deseq2_diffbound_posFC_at_TSS_1Kb.bed